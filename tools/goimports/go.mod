module gitlab.com/gitlab-org/gitaly/tools/goimports

go 1.22.0

toolchain go1.22.6

require golang.org/x/tools v0.30.0

require (
	golang.org/x/mod v0.23.0 // indirect
	golang.org/x/sync v0.11.0 // indirect
)
